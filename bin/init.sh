#!/bin/bash

set -e

CONFIG=$(readlink -f $(dirname $0)/..)
PROGRAM="xinitrc"
LINKTO=".xinitrc"
cd ~
rm -rf $LINKTO
ln -s $CONFIG/$PROGRAM $LINKTO
